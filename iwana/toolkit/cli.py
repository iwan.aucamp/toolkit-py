import logging
import argparse
import sys
import os
import os.path
import json
import jsonpickle
import posixpath
import yaml
import git
import re

from . import __version__

def collate(*args):
    for arg in args:
        if arg is not None: return arg
    return None

class Context(object):
    def __init__(self, *, arguments):
        self.arguments = arguments
        self.arguments_dict = vars(arguments)
        self.config_dir = arguments.config_dir

class GitToolkit(object):

    def __init__(self, *, context):
        self.context = context
        self.repo_path = context.arguments_dict.get("git_path", None)
        self.ids_file = collate(context.arguments_dict.get("git_ids_file", None), os.path.join(context.config_dir,"git-ids.yml"))
        self.ids = None
        if self.repo_path is not None:
            self.repo = git.Repo(self.repo_path, search_parent_directories=True)
        else:
            self.repo = git.Repo(search_parent_directories=True)

    def _get_ids(self):
        if self.ids is not None: return self.ids
        logging.debug("ids_file = %s", self.ids_file)
        if not os.path.isfile(self.ids_file): 
            self.ids = []
        else:
            with open(self.ids_file, "r") as ids_fh:
                self.ids = yaml.load(ids_fh)
        logging.debug("ids = %s", json.dumps(self.ids))
        return self.ids

    def _get_id(self,*,hint):
        for gid in self._get_ids():
            logging.debug("gid = %s", json.dumps(gid))
            if gid["email"] == hint:
                return gid
            if hint in gid["aliases"]:
                return gid
        return None

    def id_list(self):
        json.dump(self._get_ids(), sys.stdout, indent=4, sort_keys=True)
        sys.stdout.write("\n")

    stdurl_re = re.compile("^([^@]+)@([^:]+):(.+)$")
    def id_set(self,*,hint):
        gid = self._get_id(hint = hint)
        if gid is None: raise RuntimeError("Could not find id for hint {}".format(hint))
        json.dump(gid, sys.stdout, indent=4, sort_keys=True)
        sys.stdout.write("\n")
        #logging.debug("config = %s", json.dumps(self.repo.config_reader()))
        with self.repo.config_writer() as cw:
            cw.set_value("user", "email", gid["email"])
            cw.set_value("user", "name", gid["name"])
        for remote in self.repo.remotes:
            logging.debug("remote.name = %s, remote.url = %s", remote.name, remote.url)
            match = GitToolkit.stdurl_re.match(remote.url)
            logging.debug("match = %s", match)
            ssh_urls = sorted(gid["ssh_urls"].keys(), key=len, reverse=True)
            logging.debug("ssh_urls = %s", ssh_urls)
            if match:
                user = match.group(1)
                domain = match.group(2)
                path = match.group(3)
                logging.debug("user = %s, domain = %s, path = %s", user, domain, path)
                for ssh_url in ssh_urls:
                    if domain.endswith(ssh_url):
                        domain = gid["ssh_urls"][ssh_url]
                        break
                new_url = "{:s}@{:s}:{:s}".format(user, domain, path)
                logging.debug("new_url = %s", new_url)
                remote.set_url(new_url)

    def id_fromlog(self):
        head = self.repo.head
        logging.debug("head = %s, head.name = %s, head.commit = %s", head, head.name, head.commit)
        commits = list(self.repo.iter_commits())
        #logging.debug("commits = %s", commits)
        commit = commits[-1]
        logging.debug("commit = %s, commit.author = %s, commit.author.email = %s", commit, commit.author, commit.author.email)
        with self.repo.config_writer() as cw:
            cw.set_value("user", "email", commit.author.email)
            cw.set_value("user", "name", commit.author.name)
        '''
        for gid in self._get_ids():
            logging.debug("gid = %s", json.dumps(gid))
            if gid["email"] == hint:
                return gid
            if hint in gid["aliases"]:
                return gid
        return None
        '''

def make_argpath(arguments):
    elements = ["/root"]
    arguments_dict = vars(arguments)
    for key in sorted(arguments_dict.keys()):
        if key.startswith("subparser_"):
            value = arguments_dict[key]
            #elements.append("None" if value is None else value )
            if value is None: break
            elements.append("None" if value is None else value )
    return "/".join(elements)
            

def main():
    logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%dT%H:%M:%S', stream=sys.stderr, format="%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s %(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s")

    _config_dir = os.path.join(os.path.expanduser("~"),".config","iwana-toolkit")


    argument_parsers = {}
    argument_parser_path = "/root"
    argument_parsers[argument_parser_path] = argparse.ArgumentParser(add_help = False, prog="xadix-dnspod")
    argument_parsers[argument_parser_path].add_argument("--version", action="version", version="xadix-dnspod {:s}".format(__version__))
    argument_parsers[argument_parser_path].add_argument("-v", "--verbose", action="count", dest="verbosity", help="increase verbosity level")
    argument_parsers[argument_parser_path].add_argument("-h", "--help", action="help", help="shows this help message and exit")
    argument_parsers[argument_parser_path].add_argument("-c", "--config-dir", action="store", dest="config_dir", type=str, required=False, default=_config_dir, help="...")
    argument_subparsers = {}
    argument_subparsers_path = "/root"
    argument_subparsers[argument_subparsers_path] = argument_parsers[argument_parser_path].add_subparsers(dest="subparser_{:03d}".format( argument_subparsers_path.count("/") - 1 ))
    
    argument_parser_path = "/root/git"
    argument_parsers[argument_parser_path] = argument_subparsers[posixpath.dirname(argument_parser_path)].add_parser("git")
    argument_parsers[argument_parser_path].add_argument("-C", action="store", dest="git_path", type=str, required=False, default=None, help="...")
    argument_subparsers_path = "/root/git"
    argument_subparsers[argument_subparsers_path] = argument_parsers[argument_parser_path].add_subparsers(dest="subparser_{:03d}".format( argument_subparsers_path.count("/") - 1 ))

    argument_parser_path = "/root/git/id"
    argument_parsers[argument_parser_path] = argument_subparsers[posixpath.dirname(argument_parser_path)].add_parser("id")
    argument_parsers[argument_parser_path].add_argument("--file", action="store", dest="git_ids_file", type=str, required=False, help="...")
    argument_subparsers_path = "/root/git/id"
    argument_subparsers[argument_subparsers_path] = argument_parsers[argument_parser_path].add_subparsers(dest="subparser_{:03d}".format( argument_subparsers_path.count("/") - 1 ))
    argument_parser_path = "/root/git/id/list"
    argument_parsers[argument_parser_path] = argument_subparsers[posixpath.dirname(argument_parser_path)].add_parser("list")
    argument_parser_path = "/root/git/id/set"
    argument_parsers[argument_parser_path] = argument_subparsers[posixpath.dirname(argument_parser_path)].add_parser("set")
    argument_parsers[argument_parser_path].add_argument("git_id_hint", action="store", type=str, help="...")
    argument_parser_path = "/root/git/id/fromlog"
    argument_parsers[argument_parser_path] = argument_subparsers[posixpath.dirname(argument_parser_path)].add_parser("fromlog")
    #argument_parsers[argument_parser_path].set_defaults(func=git_toolkit.id_list)

    # git id list
    # git id set iwana@concurrent.systems
    # git id set iwana@

    arguments = argument_parsers["/root"].parse_args( args = sys.argv[1:] )

    if arguments.verbosity is not None:
        root_logger = logging.getLogger("")
        new_level = ( root_logger.getEffectiveLevel() - (min(1,arguments.verbosity))*10 - min(max(0,arguments.verbosity - 1),9)*1 )
        root_logger.setLevel( new_level )
        root_logger.propagate = True

    #logging.debug("argument_parser = %s", json.dumps(json.loads(jsonpickle.encode(argument_parser)), sort_keys=True, indent=4))
    logging.debug("sys.argv = %s, arguments = %s, logging.level = %s", sys.argv, arguments, logging.getLogger("").getEffectiveLevel())

    argpath = make_argpath(arguments)
    logging.debug("argpath = %s", argpath)

    context = Context(arguments=arguments)
    git_toolkit = GitToolkit(context=context)
    if False: None
    elif argpath == "/root/git/id/list":
        git_toolkit.id_list()
    elif argpath == "/root/git/id/set":
        git_toolkit.id_set(hint = arguments.git_id_hint)
    elif argpath == "/root/git/id/fromlog":
        git_toolkit.id_fromlog()
    else:
        argument_parsers[argpath].print_help()
        return 1

if __name__ == "__main__":
    main()
