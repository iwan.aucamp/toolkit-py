# https://setuptools.readthedocs.io/
# https://docs.python.org/2/distutils/index.html
# https://docs.python.org/3/distutils/index.html
import setuptools
import versioneer

# https://docs.python.org/2/distutils/setupscript.html#additional-meta-data
# https://docs.python.org/3/distutils/setupscript.html#additional-meta-data
# https://setuptools.readthedocs.io/en/latest/setuptools.html#new-and-changed-setup-keywords
# https://setuptools.readthedocs.io/en/latest/setuptools.html#metadata

setuptools.setup(
    name="iwana-toolkit",
    description="Some random tools",
    url="https://github.com/iwana/toolkit-py",
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    packages=setuptools.find_packages(),
    py_modules=[],
    entry_points={
        "console_scripts": [
            "iwana-toolkit=iwana.toolkit.cli:main",
        ]
    },
    install_requires=[
        "GitPython>=2.1.9",
        "jsonpickle>=0.9.6",
        "PyYAML>=3.12",
    ],
    zip_safe=False,
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
    ],
    python_requires='>=3, <4',
)
